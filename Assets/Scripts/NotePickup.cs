using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NotePickup : MonoBehaviour
{
    private bool scriptPlaying = false;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.name.Equals("Player") && !this.scriptPlaying)
        {
            StartCoroutine(RunTextScript(collision.gameObject));

        }

    }

    private IEnumerator RunTextScript(GameObject player)
    {
        this.scriptPlaying = true;
        TextParticleSpawner spawn = FindObjectOfType<TextParticleSpawner>();
        spawn.SpawnTextParticle("HEY LOOK", player.transform.position, new Vector2(0, 1), 0.01f, 2);

        yield return new WaitForSeconds(2);

        spawn.SpawnTextParticle("IT IS A NOTE FROM GRANDPA", player.transform.position, new Vector2(0, 1), 0.01f, 3);

        yield return new WaitForSeconds(3);

        spawn.SpawnTextParticle("REMBER YOUR WEAPON WORKS IN ALL DIRECTIONS", player.transform.position, new Vector2(0, 1), 0.01f, 5);

        yield return new WaitForSeconds(5);

        spawn.SpawnTextParticle("AND USE THE DISADVANTGE OF THE WEAPON FOR YOUR ADVANTAGE", player.transform.position, new Vector2(0, 1), 0.01f, 6);

        yield return new WaitForSeconds(5);

        spawn.SpawnTextParticle("THAT WILL MAKE YOU REACH LONGER", player.transform.position, new Vector2(0, 1), 0.01f, 4);

        this.scriptPlaying = false;
    }

}
