using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class StartGame : MonoBehaviour
{
    [SerializeField] private AudioSource introMusic;
    private bool triggered = false;
    private bool canStart = false;
    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(DelayStart());   
    }

    private void FixedUpdate()
    {
        if (triggered)
        {
            if (this.introMusic.volume > 0)
            {
                this.introMusic.volume -= 0.003f;
            }
            else
            {
                SceneManager.LoadScene("StartScene");
            }

        }

    }

    // Update is called once per frame
    void Update()
    {
        if (!this.canStart) return;

        if (Input.GetButton("Jump"))
        {
            this.triggered = true;
        }
    }

    private IEnumerator DelayStart()
    {
        yield return new WaitForSeconds(1f);
        this.canStart = true;
    }
}
