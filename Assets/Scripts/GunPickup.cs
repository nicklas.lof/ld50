using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GunPickup : MonoBehaviour
{
    private bool scriptPlaying = false;
    [SerializeField] private AudioSource pickupAudio;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }


    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.name.Equals("Player") && !this.scriptPlaying)
        {
            StartCoroutine(RunTextScript(collision.gameObject));
            collision.transform.Find("Gun").gameObject.SetActive(true);
            GetComponent<SpriteRenderer>().enabled = false;

            if (this.pickupAudio != null)
            {
                this.pickupAudio.Play();
            }


            
        }

    }

    private IEnumerator RunTextScript(GameObject player)
    {
        this.scriptPlaying = true;
        Entity playerEntity = GetComponent<Entity>();
        TextParticleSpawner spawn = FindObjectOfType<TextParticleSpawner>();

        spawn.SpawnTextParticle("HEY LOOK", player.transform.position, new Vector2(0, 1), 0.01f, 2);

        yield return new WaitForSeconds(2);

        spawn.SpawnTextParticle("IT IS GRANDPAS OLD GUN", player.transform.position, new Vector2(0, 1), 0.01f, 3);

        yield return new WaitForSeconds(3);

        spawn.SpawnTextParticle("HE ALWAYS SAID TO BE CAREFUL WITH IT", player.transform.position, new Vector2(0, 1), 0.01f, 5);

        yield return new WaitForSeconds(5);

        spawn.SpawnTextParticle("PRESS   K   TO SHOOT", player.transform.position, new Vector2(0, 1), 0.01f, 4);

       
    }
}
