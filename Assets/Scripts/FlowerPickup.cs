using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FlowerPickup : MonoBehaviour
{
    private bool scriptPlaying = false;
    [SerializeField] private AudioSource pickupAudio;
    private AudioSource gameMusic;
    private AudioSource endMusic;
    private bool fadeMusic = false;

    private void Start()
    {
        this.gameMusic = GameObject.Find("Game music").GetComponent<AudioSource>();
        this.endMusic = GameObject.Find("End music").GetComponent<AudioSource>();
    }

    private void FixedUpdate()
    {
        if (!fadeMusic) return;
        if (this.gameMusic.volume > 0) this.gameMusic.volume -= 0.001f;

        if (this.gameMusic.volume <= 0 && !this.endMusic.isPlaying) this.endMusic.Play();
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.name.Equals("Player") && !this.scriptPlaying)
        {
            StartCoroutine(RunTextScript(collision.gameObject));
            collision.transform.Find("Flower").gameObject.SetActive(true);
            GetComponent<SpriteRenderer>().enabled = false;

            if (this.pickupAudio != null)
            {
                this.pickupAudio.Play();
            }

            this.fadeMusic = true;
            Destroy(this.gameObject, 18);
        }

    }

    private IEnumerator RunTextScript(GameObject player)
    {
        this.scriptPlaying = true;
        TextParticleSpawner spawn = FindObjectOfType<TextParticleSpawner>();
        spawn.SpawnTextParticle("HEY LOOK", player.transform.position, new Vector2(0, 1), 0.01f, 2);

        yield return new WaitForSeconds(2);

        spawn.SpawnTextParticle("IT IS THE FLOWER THAT WILL SAVE GRANDMA", player.transform.position, new Vector2(0, 1), 0.01f, 3);

        yield return new WaitForSeconds(3);

        spawn.SpawnTextParticle("LETS GO BACK TO HER HOUSE", player.transform.position, new Vector2(0, 1), 0.01f, 5);

        yield return new WaitForSeconds(5);

        spawn.SpawnTextParticle("BUT I NEED TO GO TROUGH THE MOUNTAINS", player.transform.position, new Vector2(0, 1), 0.01f, 4);

        yield return new WaitForSeconds(5);

        //this.scriptPlaying = false;
    }

}
