using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Snow : MonoBehaviour
{
    private ParticleSystemForceField psforceField;
    private AreaEffector2D areaEffector;
    [SerializeField] private float lerpValue = 1f;

    [SerializeField] private float startValue = 6.5f;
    [SerializeField] private float goalValue = -6.5f;

    [SerializeField] private bool windChangeDirections = true;

    // Start is called before the first frame update
    void Start()
    {
        this.psforceField = GetComponent<ParticleSystemForceField>();
        this.areaEffector = GetComponent<AreaEffector2D>();
        if (windChangeDirections) InvokeRepeating("changeWind", 0f, 10f);

    }

    private void FixedUpdate()
    {
        var v = this.psforceField.directionX;
        var value = v.constantMax;
        this.areaEffector.forceMagnitude = value * 20;


        //if (lerpValue >= 1 && Random.Range(0f,60f) < 0.1f)
        //{
           
       // }


        if (lerpValue < 1)
        {
            lerpValue += Time.fixedDeltaTime/4f;
            float windValue = Mathf.Lerp(startValue, goalValue, lerpValue);
            var dir = this.psforceField.directionX;
            dir.constantMax = windValue;
            this.psforceField.directionX = dir;
        }
    }

    private void changeWind()
    {
        Debug.Log("Changing wind");
        if (goalValue == 6.5f)
        {
            goalValue = -6.5f;
            startValue = 6.5f;
        }
        else if (goalValue == -6.5f)
        {
            goalValue = 6.5f;
            startValue = -6.5f;
        }
        lerpValue = 0;
    }
}
