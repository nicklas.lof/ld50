using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GrandmaEnd : MonoBehaviour
{
    private bool scriptPlaying = false;

    [SerializeField] private GameObject thanksForPlaying;
    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        if (!scriptPlaying)
        {
            StartCoroutine(RunTextScript(transform.gameObject));
        }
    }
    private IEnumerator RunTextScript(GameObject target)
    {
        this.scriptPlaying = true;
        TextParticleSpawner spawn = FindObjectOfType<TextParticleSpawner>();
        spawn.SpawnTextParticle("ZZZZZZ", target.transform.position, new Vector2(Random.Range(-0.4f, 0.4f), 1), 0.01f, 3);

        yield return new WaitForSeconds(3);

        spawn.SpawnTextParticle("COUGH COUGH COUGH", target.transform.position, new Vector2(Random.Range(-0.4f, 0.4f), 1), 0.01f, 3);

        yield return new WaitForSeconds(3);

        spawn.SpawnTextParticle("ZZZZZ", target.transform.position, new Vector2(Random.Range(-0.4f, 0.4f), 1), 0.01f, 3);

        yield return new WaitForSeconds(3);

        spawn.SpawnTextParticle("HEY KID   YOU DID IT!", target.transform.position, new Vector2(Random.Range(-0.2f, 0.2f), 1), 0.009f,5);

        yield return new WaitForSeconds(5);


        spawn.SpawnTextParticle("WITH THIS FLOWER I WILL STAY BETTER FOR A FEW MORE DAYS!", target.transform.position, new Vector2(Random.Range(-0.2f, 0.2f), 1), 0.009f, 8);

        yield return new WaitForSeconds(8);

        spawn.SpawnTextParticle("COME HERE AND I WILL TELL YOU A STORY ABOUT THE MONSTERS IN THE WOODS.....", target.transform.position, new Vector2(Random.Range(-0.2f, 0.2f), 1), 0.008f, 10);

        yield return new WaitForSeconds(10);

        thanksForPlaying.SetActive(true);

    }
}
