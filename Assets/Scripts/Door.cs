using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Door : MonoBehaviour
{
    private AudioSource endMusic;
    private bool triggered = false;

    private void Start()
    {
        this.endMusic = GameObject.Find("End music").GetComponent<AudioSource>();
    }


    private void FixedUpdate()
    {
        if (triggered)
        {
            if (this.endMusic.volume > 0)
            {
                this.endMusic.volume -= 0.003f;
            }
            else
            {
                SceneManager.LoadScene("EndScene");
            }
                
        }

    }

    private void OnTriggerEnter2D(Collider2D collision)
    {

        this.triggered = true;
        GameObject.Find("Player").GetComponent<Entity>().FreezeEntity();
        GameObject.Find("Player").GetComponent<Entity>().SetMovement(Vector2.zero);
    }
}
