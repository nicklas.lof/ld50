using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bridge : MonoBehaviour
{

    [SerializeField] private bool dissapearing;
    [SerializeField] private bool obstacle;
    [SerializeField] private float timeBeforeAppearingAgain = 2f;



    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.layer == 20 && this.dissapearing)
        {
            StartCoroutine(dissapear());
        }

        if (collision.gameObject.layer == 22 && this.obstacle)
        {
            Destroy(this.gameObject);
        }
    }

    private IEnumerator dissapear()
    {
        yield return new WaitForSeconds(0.3f);

        GetComponent<SpriteRenderer>().enabled = false;
        GetComponent<Rigidbody2D>().simulated = false;

        yield return new WaitForSeconds(timeBeforeAppearingAgain);

        GetComponent<SpriteRenderer>().enabled = true;
        GetComponent<Rigidbody2D>().simulated = true;
    }
}
