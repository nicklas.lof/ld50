using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimationController : MonoBehaviour
{
    private Entity entity;
    private Animator animator;
    [SerializeField] private State state;

    enum State
    {
        idle,
        run,
        jump,
        dash,
        fall
    }
    // Start is called before the first frame update
    void Start()
    {
        this.entity = GetComponent<Entity>();
        this.animator = GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {

        Vector2 moveVector = this.entity.GetMoveVector();
        Vector2 velocity = this.entity.GetVelocity();
        bool isDashing = this.entity.GetIsDashing();

        if (moveVector.magnitude > 0) state = State.run;
        if (moveVector.magnitude == 0) state = State.idle;


        if(moveVector.magnitude != 0 && (velocity.y > 0.05f || velocity.y < -0.05f)) state = State.jump;
        if (velocity.y < -0.1f) state = State.fall;

        if (isDashing) state = State.dash;

        //Debug.Log(velocity);


        foreach (var state in Enum.GetValues(typeof(State)))
        {
            this.animator.SetBool(state.ToString(), false);
        }


        switch (state)
        {
            case State.idle:
                this.animator.SetBool(State.idle.ToString(),true);
                break;
            case State.run:
                this.animator.SetBool(State.run.ToString(), true);
                break;
            case State.dash:
                this.animator.SetBool(State.dash.ToString(), true);
                break;
            case State.fall:
                this.animator.SetBool(State.fall.ToString(), true);
                break;
            case State.jump:
                this.animator.SetBool(State.jump.ToString(), true);
                break;

        }
    }
}
