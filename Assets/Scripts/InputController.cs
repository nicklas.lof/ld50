using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InputController : MonoBehaviour
{

    private Vector2 moveVector = new Vector2();
    private Vector2 upDownVector = new Vector2();
    private Entity entity;

    // Start is called before the first frame update
    void Start()
    {
        this.entity = GetComponent<Entity>();
    }

    // Update is called once per frame
    void Update()
    {
        float v = Input.GetAxisRaw("Vertical");
        float h = Input.GetAxisRaw("Horizontal");

        bool jump = Input.GetButtonDown("Jump");

        bool dash = Input.GetButtonDown("Dash");

        bool fireWeapon = Input.GetButtonDown("FireWeapon");

        moveVector.x = h;
        moveVector.y = 0;

        upDownVector.x = h;
        upDownVector.y = v;

        this.entity.SetMovement(this.moveVector);
        this.entity.SetLookUpOrDown(upDownVector);

        if (jump) this.entity.Jump();
        if (dash) this.entity.Dash();
        if (fireWeapon) this.entity.Fire();

    }
}
