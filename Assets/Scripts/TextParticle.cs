using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TextParticle : MonoBehaviour
{

    [SerializeField] public float speed;
    [SerializeField] public Vector2 direction;
    [SerializeField] public float ttl;
    // Start is called before the first frame update
    void Start()
    {
        Destroy(this.gameObject, ttl);
    }

    private void FixedUpdate()
    {
        Vector2 directionAndSpeed = direction * speed;

        Vector2 particlePosition = this.transform.position;

        particlePosition += directionAndSpeed;

        this.transform.position = particlePosition;
    }
}
