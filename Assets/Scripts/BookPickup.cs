using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BookPickup : MonoBehaviour
{
    private bool scriptPlaying = false;
    [SerializeField] private AudioSource pickupAudio;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.name.Equals("Player") && !this.scriptPlaying)
        {
            StartCoroutine(RunTextScript(collision.gameObject));
            collision.gameObject.GetComponent<Entity>().hasDashAbility = true;
            GetComponent<SpriteRenderer>().enabled = false;

            if (this.pickupAudio != null)
            {
                this.pickupAudio.Play();
            }


            Destroy(this.gameObject, 18);
        }

    }

    private IEnumerator RunTextScript(GameObject player)
    {
        this.scriptPlaying = true;
        TextParticleSpawner spawn = FindObjectOfType<TextParticleSpawner>();
        spawn.SpawnTextParticle("HEY LOOK", player.transform.position, new Vector2(0, 1), 0.01f, 2);

        yield return new WaitForSeconds(2);

        spawn.SpawnTextParticle("IT IS GRANDMAS BOOK", player.transform.position, new Vector2(0, 1), 0.01f, 3);

        yield return new WaitForSeconds(3);

        spawn.SpawnTextParticle("IT SAYS I CAN DASH WHILE JUMPING", player.transform.position, new Vector2(0, 1), 0.01f, 5);

        yield return new WaitForSeconds(5);

        spawn.SpawnTextParticle("SO I CAN ACCESS AREAS FAR AWAY", player.transform.position, new Vector2(0, 1), 0.01f, 4);

        yield return new WaitForSeconds(5);

        spawn.SpawnTextParticle("PRESS  J  TO DASH WHILE JUMPING", player.transform.position, new Vector2(0, 1), 0.01f, 4);

        this.scriptPlaying = false;
    }

}
