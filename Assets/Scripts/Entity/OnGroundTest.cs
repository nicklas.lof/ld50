using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OnGroundTest : MonoBehaviour
{
    private Entity entity;
    [SerializeField] private AudioSource landAudio;
    // Start is called before the first frame update
    void Start()
    {
        this.entity = GetComponentInParent<Entity>();
    }


    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (isLayerGround(collision))
        {
            //if (landAudio != null && !this.entity.GetOnGround() && this.entity.health > 0)
           // {
          //      landAudio.Play();
          //  }
            this.entity.SetOnGround(true);
           
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (isLayerGround(collision)) this.entity.SetOnGround(false);
    }

    private void OnTriggerStay2D(Collider2D collision)
    {
        if (isLayerGround(collision)) this.entity.SetOnGround(true);
    }



    private static bool isLayerGround(Collider2D collision)
    {
        return collision.gameObject.layer == 10 || collision.gameObject.layer == 11;
    }
}
