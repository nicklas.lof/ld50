using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;




public class Entity : MonoBehaviour
{
    enum FacingDirection
    {
        left,
        right
    }

    private bool entityFrozen;
    [SerializeField] private float moveSpeed;
    [SerializeField] private float jumpSpeed;
    [SerializeField] private float dashSpeed;
    [SerializeField] private float knockbackSpeed;
    [SerializeField] private float dashDuration;
    [SerializeField] private float fireDuration;



    [SerializeField] public int health;
    private Rigidbody2D rb;
    [SerializeField] private List<SpriteRenderer> spriteRenders = new List<SpriteRenderer>();
    private Vector2 moveVector;



    [SerializeField] private bool onGround;

    [SerializeField] public bool canJump = true;



    [SerializeField] private bool canFire = true;
    [SerializeField] private bool isKnockbacked;
    [SerializeField] private bool doJump;
    [SerializeField] private bool doFire;
    private bool doDash;


    private bool canDash = true;
    [SerializeField] private bool isDashing = false;

    [SerializeField] FacingDirection facingDirection = FacingDirection.right;



    [SerializeField] private GameObject dashParticlesRight;


    [SerializeField] private GameObject dashParticlesLeft;

    [SerializeField] private GameObject fireParticlesLeft;
    [SerializeField] private GameObject fireParticlesRight;

    [SerializeField] private GameObject onDeathParticles;
    [SerializeField] private GameObject onHitParticles;

    [SerializeField] private GameObject bullet;

    [SerializeField] private AudioSource jumpAudio;
    [SerializeField] private AudioSource dashAudio;
    [SerializeField] private AudioSource gunAudio;
    [SerializeField] private AudioSource deathAudio;

    private Vector2 upOrDownVector;
    private bool verticalKnock;
    [SerializeField] internal bool hasDashAbility = false;
    private bool canBeHit = true;
    public bool deathParticlesShown;



    // Start is called before the first frame update
    void Start()
    {
        this.rb = GetComponent<Rigidbody2D>();
        this.spriteRenders.AddRange(GetComponents<SpriteRenderer>());
        this.spriteRenders.AddRange(GetComponentsInChildren<SpriteRenderer>());
    }



    internal void SetOnGround(bool onGround)
    {
        this.onGround = onGround;
        this.canDash = true;
        this.isDashing = false;
    }

    private void FixedUpdate()
    {
        if (entityFrozen) return;
        if (this.health <= 0)
        {
            this.rb.velocity = Vector2.zero;
            return;
        }
            if (!this.isDashing && !this.isKnockbacked)
        {
            Vector2 move = this.rb.velocity;
            move.x = this.moveVector.x;
            this.rb.velocity = move;
        }

        if (this.verticalKnock)
        {
            Vector2 move = this.rb.velocity;
            move.x = this.moveVector.x;
            this.rb.velocity = move;
        }


        if (doJump && onGround && canJump)
        {
            this.doJump = false;
            this.rb.AddForce(new Vector2(0, jumpSpeed), ForceMode2D.Impulse);
            if (this.jumpAudio != null)
            {
                this.jumpAudio.Play();
            }
            StartCoroutine(JumpDelay());
        }

        if (doDash && !onGround && canDash)
        {
            this.doDash = false;
            this.isDashing = true;
            this.canDash = false;

            Vector2 dashDirection = new Vector2();
            GameObject particles = null;

            switch (facingDirection)
            {
                case FacingDirection.left:
                    dashDirection.x = -dashSpeed;
                    particles = this.dashParticlesRight;
                    break;
                case FacingDirection.right:
                    dashDirection.x = dashSpeed;
                    particles = this.dashParticlesLeft;
                    break;
            }

            this.rb.AddForce(dashDirection, ForceMode2D.Impulse);
            if (this.dashAudio != null)
            {
                this.dashAudio.Play();
            }

            GameObject p = Instantiate(particles, transform.position, Quaternion.identity);
            p.transform.SetParent(this.transform);

          
            StartCoroutine(DashDelay());

        }

        

        if (doFire && canFire)
        {
            this.doFire = false;
            this.canFire = false;
            this.isKnockbacked = true;

            Vector2 knockbackDirection = new Vector2();
            Vector2 bulletDirection = new Vector2();
            
            GameObject particles = null;

            switch (facingDirection)
            {
                case FacingDirection.left:
                    knockbackDirection.x = knockbackSpeed;
                    bulletDirection.x = -1;
                    particles = this.fireParticlesRight;
                    break;
                case FacingDirection.right:
                    knockbackDirection.x = -knockbackSpeed;
                    bulletDirection.x = 1;
                    particles = this.fireParticlesLeft;
                    break;
            }



            if (transform.Find("Gun") && transform.Find("Gun").gameObject.activeSelf)
            {
                Transform hole = transform.Find("Gun").Find("Hole");
                GameObject p = Instantiate(particles, hole.position, Quaternion.identity);
                GameObject b = Instantiate(bullet, hole.position, Quaternion.identity);
                Bullet bulletScript = b.GetComponent<Bullet>();

                if (this.upOrDownVector.y > 0.8f)
                {
                    bulletDirection.x = 0;
                    bulletDirection.y = 1;
                    if (this.rb.velocity.y < 0.1f || this.rb.velocity.y > -0.1f)
                    {
                        knockbackDirection.x = 0;
                        knockbackDirection.y = -knockbackSpeed * 1.2f;
                        this.verticalKnock = true;
                    }
                    else
                    {
                        knockbackDirection.x = 0;
                        knockbackDirection.y = -knockbackSpeed;
                    }

                    b.transform.localEulerAngles = new Vector3(0, 0, 90);
                }

                else if (this.upOrDownVector.y < -0.8f)
                {
                    bulletDirection.x = 0;
                    bulletDirection.y = -1;
                    if (this.rb.velocity.y < 0.1f || this.rb.velocity.y > -0.1f)
                    {
                        knockbackDirection.x = 0;
                        knockbackDirection.y = knockbackSpeed * 1.2f;
                        this.verticalKnock = true;
                    }
                    else
                    {
                        knockbackDirection.x = 0;
                        knockbackDirection.y = -knockbackSpeed;
                    }

                    b.transform.localEulerAngles = new Vector3(0, 0, -90);
                }
                bulletScript.direction = bulletDirection;
                bulletScript.ttl = 0.5f;
                bulletScript.speed = 15;

                this.rb.AddForce(knockbackDirection, ForceMode2D.Impulse);

                if (this.gunAudio != null)
                {
                    this.gunAudio.Play();
                }


            }

            StartCoroutine(FireDelay());
            StartCoroutine(KnockbackDelay());

        }


        if (isDashing || isKnockbacked)
        {
            Vector2 move = this.rb.velocity;
            if (!this.verticalKnock)
            {
                move.y = 0;
            }

            this.rb.velocity = move;
        }

    }

    private void Update()
    {
        if (entityFrozen) return;
        if (this.health <= 0)
        {

            if (GetComponent<SpriteRenderer>().enabled) // Blah ugly.. last "minute" thing.. :)
            {
                if (this.deathAudio != null)
                {
                    this.deathAudio.Play();
                }
            }

            if (!this.name.Equals("Player")) Destroy(this.gameObject);
            else GetComponent<SpriteRenderer>().enabled = false;

            if (this.onDeathParticles != null)
            {
                if (!this.deathParticlesShown)
                {
                    GameObject p = Instantiate(this.onDeathParticles, transform.position, Quaternion.identity);
                    p.transform.SetParent(null);
                    this.deathParticlesShown = true;
                }

            }
        }

        if (this.health <= 0)
        {
            this.rb.velocity = Vector2.zero;
            return;
        }

        if (this.moveVector.x < -0.1f) this.facingDirection = FacingDirection.left;
        if (this.moveVector.x > 0.1f) this.facingDirection = FacingDirection.right;

        switch (facingDirection)
        {
            case FacingDirection.left:
                /*foreach (var sr in this.spriteRenders)
                {
                    sr.flipX = true;
                }*/
                this.transform.eulerAngles = new Vector3(0, 180, 0);
                break;
            case FacingDirection.right:
                this.transform.eulerAngles = new Vector3(0, 0, 0);
                /*foreach (var sr in this.spriteRenders)
                {
                    sr.flipX = false;
                }*/
                break;
        }
        if (transform.Find("Gun") && transform.Find("Gun").gameObject.activeSelf)
        {
            if (this.upOrDownVector.y > 0.8f)
            {
                Transform gun = transform.Find("Gun");
                gun.localEulerAngles = new Vector3(0,  0, 90);
            }

            else if (this.upOrDownVector.y < -0.8f)
            {
                Transform gun = transform.Find("Gun");
                gun.localEulerAngles = new Vector3(0, 0, -90);
            }
            else
            {
                Transform gun = transform.Find("Gun");
                gun.localEulerAngles = new Vector3(0, 0, 0);
            }
        }
    }

    internal void Dash()
    {
        if (this.health <= 0) return;
        if (this.hasDashAbility && this.canDash && !this.onGround) this.doDash = true;
    }

    internal void SetMovement(Vector2 moveVector)
    {
        if (entityFrozen) return;
        if (this.health <= 0)
        {
            this.moveVector = Vector2.zero;
            return;
        }
        this.moveVector = moveVector;
        this.moveVector.x *= moveSpeed;
    }

    internal void SetLookUpOrDown(Vector2 vector)
    {
        this.upOrDownVector = vector;
    }

    internal void Jump()
    {
        if (entityFrozen) return;
        if (this.health <= 0) return;
        if (this.canJump) this.doJump = true;
    }

    internal void Fire()
    {
        if (entityFrozen) return;
        if (this.health <= 0) return;
        if (this.canFire) this.doFire = true;
    }


    internal void FreezeEntity()
    {
        this.entityFrozen = true;
        this.rb.velocity = Vector2.zero;
        this.moveVector = Vector2.zero;
    }


    internal void DecreaseHealth(int health)
    {
        if (this.canBeHit)
        {
            this.health -= health;
            if (this.onHitParticles)
            {
                GameObject p = Instantiate(this.onHitParticles, transform.position, Quaternion.identity);
                p.transform.SetParent(null);
            }

            StartCoroutine(HitDelay());
        }
        
    }

    private IEnumerator JumpDelay()
    {
        this.canJump = false;

        yield return new WaitForSeconds(0.1f);
        this.canJump = true;
    }

    private IEnumerator DashDelay()
    {
        yield return new WaitForSeconds(dashDuration);
        this.isDashing = false;
    }

    private IEnumerator FireDelay()
    {
        this.canFire = false;
        yield return new WaitForSeconds(fireDuration);
        this.canFire = true;
    }

    private IEnumerator KnockbackDelay()
    {
        yield return new WaitForSeconds(0.2f);
        this.isKnockbacked = false;
        this.verticalKnock = false;
    }

    private IEnumerator HitDelay()
    {
        this.canBeHit = false;

        yield return new WaitForSeconds(0.3f);
        this.canBeHit = true;
    }

    internal Vector2 GetVelocity()
    {
        return this.rb.velocity;
    }

    internal Vector2 GetMoveVector()
    {
        return this.moveVector;
    }

    internal bool GetIsDashing()
    {
        return this.isDashing;
    }

    internal bool GetOnGround()
    {
        return this.onGround;
    }

}
