using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Eyemonster : MonoBehaviour
{
    private GameObject player;
    private Entity entity;

    // Start is called before the first frame update
    void Start()
    {
        this.entity = GetComponent<Entity>();
    }

    // Update is called once per frame
    void Update()
    {

        if (this.player == null)
        {
            this.player = GameObject.Find("Player").gameObject;
        }

        Vector3 playerPosition = this.player.transform.position;

        Vector3 distance = playerPosition - this.transform.position;

        float distanceToPlayer = distance.magnitude;

        if (distanceToPlayer < 5)

        {
            Debug.DrawLine(this.transform.position, this.transform.position + distance, Color.red, 1f);
            int mask = LayerMask.GetMask("Player", "Ground");
            RaycastHit2D hit = Physics2D.Raycast(this.transform.position, distance,distanceToPlayer,mask);

            //Debug.Log(hit.transform.gameObject.name);
            
            //Debug.Log(this.transform.position + distance + " "+ (this.transform.position));

            if (hit.transform != null && hit.transform.gameObject.name.Equals("Player"))
            {
                Vector2 moveVector = distance.normalized;

                entity.SetMovement(moveVector);
            }



        }
        else
        {
            entity.SetMovement(Vector2.zero);
        }

    }


    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.transform.gameObject.layer == 22)
        {
            this.entity.DecreaseHealth(1);
        }

        if (collision.transform.gameObject.layer == 20)
        {
            collision.gameObject.GetComponent<Entity>().DecreaseHealth(1);
        }

    }
}
