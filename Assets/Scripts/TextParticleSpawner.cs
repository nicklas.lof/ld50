using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TextParticleSpawner : MonoBehaviour
{

    [SerializeField] private GameObject textTemplate;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }


    public GameObject SpawnTextParticle(string text, Vector2 position, Vector2 direction, float speed, float ttl)
    {
        GameObject particle = Instantiate(textTemplate, position, Quaternion.identity);
        particle.transform.SetParent(this.transform);
        particle.transform.localScale = new Vector3(1, 1, 1);

        var tmp = particle.GetComponent<TMPro.TMP_Text>();
        tmp.text = text;

        TextParticle textParticle = particle.GetComponent<TextParticle>();

        textParticle.ttl = ttl;
        textParticle.direction = direction;
        textParticle.speed = speed;
        return particle;
    }
}
