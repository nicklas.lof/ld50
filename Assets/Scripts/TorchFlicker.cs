using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Rendering.Universal;

public class TorchFlicker : MonoBehaviour
{
    private Light2D light2d;
    private float nextFlicker;
    private float oldValue;
    private float newValue;


    // Start is called before the first frame update
    void Start()
    {
        this.light2d = GetComponentInChildren<UnityEngine.Rendering.Universal.Light2D>();
        this.nextFlicker = Random.Range(0.3f, 1f);
        this.oldValue = 0.5f;
        this.newValue = Random.Range(0.80f, 1f);
}

    // Update is called once per frame
    void FixedUpdate()
    {
        this.nextFlicker -= Time.fixedDeltaTime;

        if (this.nextFlicker <= 0)
        {
            this.nextFlicker = Random.Range(0.3f, 1f);
            oldValue = this.newValue;
            this.newValue = Random.Range(0.8f, 1f);
        }

        float value = Mathf.Lerp(oldValue, newValue, nextFlicker);
        this.light2d.intensity = value;
    }
}
