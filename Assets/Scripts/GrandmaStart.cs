using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GrandmaStart : MonoBehaviour
{
    private bool scriptPlaying = false;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (!scriptPlaying)
        {
            StartCoroutine(RunTextScript(transform.gameObject));
        }
    }
    private IEnumerator RunTextScript(GameObject target)
    {
        this.scriptPlaying = true;
        TextParticleSpawner spawn = FindObjectOfType<TextParticleSpawner>();
        spawn.SpawnTextParticle("ZZZZZZ", target.transform.position, new Vector2(Random.Range(-0.4f,0.4f), 1), 0.01f, 3);

        yield return new WaitForSeconds(3);

        spawn.SpawnTextParticle("COUGH COUGH COUGH", target.transform.position, new Vector2(Random.Range(-0.4f, 0.4f), 1), 0.01f, 3);

        yield return new WaitForSeconds(3);

        spawn.SpawnTextParticle("ZZZZZ", target.transform.position, new Vector2(Random.Range(-0.4f, 0.4f), 1), 0.01f, 3);

        yield return new WaitForSeconds(3);

        spawn.SpawnTextParticle("HEY KID.. GRANDMA IS NOT FEELING WELL.", target.transform.position, new Vector2(Random.Range(-0.2f, 0.2f), 1), 0.009f, 6);

        yield return new WaitForSeconds(6);


        spawn.SpawnTextParticle("I FEAR MY TIME HAS COME!", target.transform.position, new Vector2(Random.Range(-0.2f, 0.2f), 1), 0.009f, 5);

        yield return new WaitForSeconds(5);

        spawn.SpawnTextParticle("BUT HEY KID YOU CAN HELP ME.  DEEP IN THE MOUTAINS THERE SHOULD BE A HEALING FLOWER.", target.transform.position, new Vector2(Random.Range(-0.2f, 0.2f), 1), 0.008f, 10);

        yield return new WaitForSeconds(10);

        spawn.SpawnTextParticle("IF YOU CAN GET IT FOR ME I MIGHT BE BETTER FOR A WHILE.", target.transform.position, new Vector2(Random.Range(-0.2f, 0.2f), 1), 0.008f, 10);

        yield return new WaitForSeconds(10);

        spawn.SpawnTextParticle("ZZZZZZ", target.transform.position, new Vector2(Random.Range(-0.2f, 0.2f), 1), 0.01f, 2);

        yield return new WaitForSeconds(2);

        spawn.SpawnTextParticle("COUGH COUGH COUGH", target.transform.position, new Vector2(Random.Range(-0.2f, 0.2f), 1), 0.01f, 2);

        yield return new WaitForSeconds(3);


        SceneManager.LoadScene("Game");


    }


    
}
