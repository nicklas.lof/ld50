using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spikes : MonoBehaviour
{

    [SerializeField] private bool falling;
    [SerializeField] private Rigidbody2D spikeToFall;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (falling && collision.gameObject.layer == 20)
        {
            
            spikeToFall.simulated = true;
        }
    }


}
