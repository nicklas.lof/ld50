using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerLife : MonoBehaviour
{
    private Entity entity;
    private bool playing;

    private bool canPress = false;
    private GameObject currentMessage;
    private int messageShown;

    // Start is called before the first frame update
    void Start()
    {
        this.entity = GetComponent<Entity>();
    }

    // Update is called once per frame
    void Update()
    {
        if (this.entity.health <= 0)
        {
            if (!this.playing)
            {
                GetComponent<InputController>().enabled = false;
                this.playing = true;
                TextParticleSpawner spawn = FindObjectOfType<TextParticleSpawner>();
                this.messageShown++;
                if (messageShown < 3) this.currentMessage = spawn.SpawnTextParticle("YOU DIED. PRESS TO RESTART FROM CHECKPOINT", transform.position, new Vector2(0, 1), 0.01f, 8);
                StartCoroutine(pressDelay());
            }

        }

        if (this.playing)
        {
            if (this.canPress && Input.GetButton("Jump"))
            {
                FindObjectOfType<CheckpointController>().RestoreFromLatestCheckpoint();
                this.playing = false;
                this.canPress = false;
                Destroy(currentMessage);
            }
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.transform.gameObject.layer == 25)
        {
            killPlayer();
        }
    }

    private void killPlayer()
    {
        this.entity.DecreaseHealth(1000);
    }

    private IEnumerator pressDelay()
    {
        yield return new WaitForSeconds(0.5f);
        this.canPress = true;
    }
}
