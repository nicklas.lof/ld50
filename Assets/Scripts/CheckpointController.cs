using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CheckpointController : MonoBehaviour
{

    [SerializeField] private GameObject latestCheckpoint;


    internal void RestoreFromLatestCheckpoint()
    {
        GameObject player = GameObject.Find("Player");
        Transform restorePoint = latestCheckpoint.transform.Find("RestorePoint");



        player.transform.position = restorePoint.position;

        resetPlayer(player);

    }

    private void resetPlayer(GameObject player)
    {
        player.GetComponent<Entity>().health = 5;
        player.GetComponent<SpriteRenderer>().enabled = true;
        player.GetComponent<Entity>().SetOnGround(false);
        player.GetComponent<Entity>().deathParticlesShown = false;
        player.GetComponent<InputController>().enabled = true;

        StartCoroutine(jumpDelay(player));


    }

    internal void SaveCheckpoint(GameObject gameObject)
    {
        latestCheckpoint = gameObject;
    }

    private IEnumerator jumpDelay(GameObject player)
    {
        player.GetComponent<Entity>().canJump = false;
        yield return new WaitForSeconds(0.5f);
        player.GetComponent<Entity>().canJump = true;
    }
}
